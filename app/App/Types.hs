module App.Types where

import Types
import Control.Exception(Exception)

data AppState = AppState {
    appstate_url :: URL,
    appstate_regex :: Regex,
    appstate_results :: [URL],
    appstate_config_fonts :: AppConfigFonts
    } deriving (Show,Eq)

newState :: AppState
newState = AppState "" "" [] []

data AppEvent = AppInit
                | AppLoad AppConfigFonts
                | OnURLChange URL
                | OnRegexChange Regex
                | Results [URL]
                | NOP
                deriving (Show,Eq)

type AppConfigFonts = [String]
