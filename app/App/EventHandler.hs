module App.EventHandler where

import Monomer
import Types
import App.Types
import Utils.Maybe
import Scrape
import qualified Font.FilePath as Font

handleEvent :: WidgetEnv AppState AppEvent -> WidgetNode AppState AppEvent -> AppState -> AppEvent -> [AppEventResponse AppState AppEvent]
handleEvent _env _node _s AppInit = [Task loadFonts]
handleEvent _env _node s (AppLoad fonts) = [Model $ s{appstate_config_fonts=fonts <> appstate_config_fonts s}]
handleEvent _env _node s (OnURLChange url) = [Task . scrape $ "https://" <> url, Model s{appstate_url=url}]
handleEvent _env _node s (OnRegexChange regex) = [Model s{appstate_regex=regex}]
handleEvent _env _node s (Results urls) = [Model s{appstate_results=urls}]
handleEvent _env _node _s _ = []

loadFonts :: IO AppEvent
loadFonts = Font.firstAvailable >>= return . maybeCase NOP (AppLoad . pure)

scrape :: URL -> IO AppEvent
scrape = fmap Results . allLinks

debug :: IO AppEvent
debug = return $ Results ["!"]
