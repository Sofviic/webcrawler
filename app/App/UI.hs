{-# LANGUAGE OverloadedStrings #-}
module App.UI where

import Monomer
import App.Types
import Utils.Maybe
import Utils.Compose
import Data.Text(Text)
import qualified Data.Text as T

buildUI :: WidgetEnv AppState AppEvent -> AppState -> WidgetNode AppState AppEvent
buildUI _ s = vstack [
                labell s "Links:",
                spacer,
                separatorLine,
                spacer,
                vscroll . vstack $ do
                    url <- appstate_results s
                    [labell s $ T.pack url, spacer]
                ,
                filler,
                separatorLine,
                spacer,
                textFieldlt s (appstate_url s) OnURLChange [placeholder "www.wikipedia.org"],
                textFieldlt s (appstate_regex s) OnRegexChange [placeholder "[a-zA-Z0-9]*"]
                ] `styleBasic` [padding 10]

stateConfig_font :: AppState -> [StyleState]
stateConfig_font s = [configMaybe (headS $ appstate_config_fonts s) (textFont . Font . T.pack)]

labell :: AppState -> Text -> WidgetNode AppState AppEvent
labell s = (`styleBasic` stateConfig_font s) . label

textFieldlt :: AppState -> String -> (String -> AppEvent) -> [TextFieldCfg AppState AppEvent] -> WidgetNode AppState AppEvent
textFieldlt s t f = textFieldl s (T.pack t) (f . T.unpack)

textFieldl :: AppState -> Text -> (Text -> AppEvent) -> [TextFieldCfg AppState AppEvent] -> WidgetNode AppState AppEvent
textFieldl s = (`styleBasic` stateConfig_font s <> [height 50]) .##. textFieldV_
