{-# LANGUAGE OverloadedStrings #-}
module App.Config where

import Monomer
import App.Types
import qualified Font.FilePath as Font
import qualified Data.Text as T

config :: [AppConfig AppState AppEvent]
config = [
    appWindowTitle "Link Scraper",
    --appWindowIcon "./assets/images/icon.png",
    appTheme darkTheme,
    appFontDef "dejavu"   . T.pack $ Font.dejavu,
    appFontDef "cascadia" . T.pack $ Font.cascadia,
    appFontDef "arial"    . T.pack $ Font.arial,
    appFontDef "impact"   . T.pack $ Font.impact,
    appFontDef "consolas" . T.pack $ Font.consolas,
    appFontDef "calibri"  . T.pack $ Font.calibri,
    appInitEvent AppInit
    ]


