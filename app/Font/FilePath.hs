module Font.FilePath(
    dejavu,cascadia,arial,impact,consolas,calibri,
    firstAvailable
    ) where

import Data.Maybe(listToMaybe)
import System.Info(os)
import System.Directory(doesFileExist)

firstAvailable :: IO (Maybe FilePath)
firstAvailable = doTillP doesFileExist [
    (dejavu, "dejavu"),
    (cascadia, "cascadia"),
    (arial, "arial"),
    (impact, "impact"),
    (consolas, "consolas"),
    (calibri, "calibri")]

dejavu,cascadia,arial,impact,consolas,calibri :: FilePath
dejavu      = fontDir <> "DejaVuSansMono-Bold.ttf"
cascadia    = fontDir <> "CascadiaMono.ttf"
arial       = fontDir <> "arialbd.ttf"
impact      = fontDir <> "impact.ttf"
consolas    = fontDir <> "consolab.ttf"
calibri     = fontDir <> "calibrib.ttf"

fontDir :: FilePath
fontDir = case os of
        "darwin" -> error "/System/Library/Fonts/"
        "mingw32" -> "C:/Windows/Fonts/"
        _ -> error "/usr/share/fonts/"

doTill :: (a -> IO Bool) -> [a] -> IO (Maybe a)
doTill _ [] = return Nothing
doTill mp (a:as) = do
                    b <- mp a
                    if b 
                        then return $ Just a 
                        else doTill mp as

doTillP :: Eq a => (a -> IO Bool) -> [(a,b)] -> IO (Maybe b)
doTillP mp p = doTill mp (fst <$> p) >>= \ma -> return (listToMaybe $ snd <$> filter ((==ma) . Just . fst) p)

