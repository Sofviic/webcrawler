module Main (main) where

import Monomer
import App.Types
import App.EventHandler
import App.UI
import App.Config

main :: IO ()
main = startApp newState handleEvent buildUI config


