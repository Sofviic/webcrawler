module Utils.Flip where

flip3 :: (a -> b -> c -> z) -> c -> a -> b -> z
flip3 f a b c = f b c a

flip4 :: (a -> b -> c -> d -> z) -> d -> a -> b -> c -> z
flip4 f a b c d = f b c d a
