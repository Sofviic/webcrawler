module Utils.Maybe where

maybeCase :: b -> (a -> b) -> Maybe a -> b
maybeCase b _ Nothing = b
maybeCase _ f (Just a) = f a

headS :: [a] -> Maybe a
headS [] = Nothing
headS (a:_) = Just a
