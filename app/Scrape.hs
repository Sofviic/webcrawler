{-# LANGUAGE OverloadedStrings #-}
module Scrape where

import Data.Maybe(fromMaybe)
import Data.Either(fromRight)
import Control.Exception(IOException,handle)
import qualified Font.FilePath as Font
import Text.HTML.Scalpel

main' :: IO ()
main' = filter (not.null) . lines <$> readFile "src/root.url" 
--        >>= mapM allLinks
--        >>= writeFile "src/res.url" . unlines . fmap (unlines . (<>[""]) . fromRight [])
--        >>= writeFile "src/res.url" . unlines . fmap (unlines . (<>[""]) . fromMaybe [])
        >> Font.firstAvailable
        >>= print
        >> return ()

allLinks :: URL -> IO [URL]
allLinks url = (handle :: (IOException -> IO a) -> IO a -> IO a) (\_ -> putStrLn "NonURL" >> return []) 
        $ fromMaybe [] <$> scrapeURL url (chroots "a" $ attr "href" "a")
